`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/04/2018 07:47:07 PM
// Design Name: 
// Module Name: ALUTB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALUTB();

reg [0:63] A, B;
reg [0:3] ALU_operation;
wire Zero;
wire Invalid;
wire [0:63] ALU_result;


LEGv8ALU ALUTEST(ALU_operation, A, B, ALU_result, Zero, Invalid);
initial begin
	A = 64'h5555555555555555;
	B = 64'haaaaaaaaaaaaaaaa;
	ALU_operation <= 4'b0000;
	#1;
	ALU_operation <= 4'b0001;
	#1;
	ALU_operation <= 4'b0010;
	#1;
	ALU_operation <= 4'b0110;
	#1;
	ALU_operation <= 4'b0111;
	#1;
	ALU_operation <= 4'b1100;
	#1;
	ALU_operation <= 4'b1111;
end
endmodule
