`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/04/2018 07:47:07 PM
// Design Name: 
// Module Name: ALUTB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////////




module ALUTB_Integrated();

reg [63:0] A, B;
reg [1:0] ALU_Op;
reg [10:0] Opcode_field;
wire [3:0] ALU_operation;
wire Zero;
wire Invalid;
wire [63:0] ALU_result;

ALU_Control ALUCONTROL(ALU_Op, Opcode_field, ALU_operation);
LEGv8ALU ALUTEST(ALU_operation, A, B, ALU_result, Zero, Invalid);
initial begin
	A = 64'h5555555555555555; //every other bit a 0 0101010...
	B = 64'haaaaaaaaaaaaaaaa; //every other bit is a 1 1010101...
	ALU_Op <= 2'b00; // Expected output is all zero
	#1;
	ALU_Op <= 2'b01;
	#1;
    ALU_Op <= 2'b11;
    #1;
    ALU_Op <= 2'b10;
    Opcode_field <=11'b10001011000;
    #1;
    Opcode_field <=11'b11001011000;
    #1;
    Opcode_field <=11'b10001010000;
    #1;
    Opcode_field <=11'b10101010000;
    #1;
  
//	ALU_operation <= 4'b0000;
//	#1;
//	ALU_operation <= 4'b0001;
//	#1;
//	ALU_operation <= 4'b0010;
//	#1;
//	ALU_operation <= 4'b0110;
//	#1;
//	ALU_operation <= 4'b0111;
//	#1;
//	ALU_operation <= 4'b1100;
//	#1;
//	ALU_operation <= 4'b1111;
end
endmodule
