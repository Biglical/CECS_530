`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/11/2018 03:48:52 PM
// Design Name: 
// Module Name: ALU_Control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_Control(ALU_Op, Opcode_field, ALU_operation);

    input [1:0] ALU_Op;
    input [10:0] Opcode_field;
    output reg [3:0] ALU_operation;
    
always @ (ALU_Op,Opcode_field)begin
            casez(ALU_Op)
            2'b00: ALU_operation <= 4'b1100;
            2'b01: ALU_operation <= 4'b0111;
            2'b1?:
                case(Opcode_field) 
                11'b10001011000: ALU_operation <= 4'b0010;
                11'b11001011000: ALU_operation <= 4'b0110;
                11'b10001010000: ALU_operation <= 4'b0000;
                11'b10101010000: ALU_operation <= 4'b0001;
                default: ALU_operation <= 4'b1111;
                endcase
            default: ALU_operation <= 4'b1111;
            endcase
        end
endmodule
