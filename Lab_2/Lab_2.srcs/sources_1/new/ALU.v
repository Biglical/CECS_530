`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Jeremy Blaire
// 
// Create Date: 09/11/2018 07:50:46 PM
// Module Name: four2one_Mux
// Project Name: Lab 2
// Description: Basic ALU with Zero flag and Error Flag
// 
// 
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////

module LEGv8ALU (ALU_operation, A, B, ALU_result, Zero, Error_Flag);
	input [0:3] ALU_operation;
	input [0:63] A;
	input [0:63] B;
	output reg [0:63] ALU_result;
	output Zero;
	output Error_Flag; //rename to Error_Flag

reg error_flag;
	always @ (ALU_operation, A, B)begin
	    error_flag = 0; //set error flag to zero asuming no error
	    case(ALU_operation)
	    4'b0000:ALU_result = A & B; 
	    4'b0001:ALU_result = A | B;
	    4'b0010:ALU_result = A + B;
	    4'b0110:ALU_result = A - B;
	    4'b0111:ALU_result = B;
	    4'b1100:ALU_result = ~(A | B);
	  
	    default:begin 
	            error_flag = 1;
	            ALU_result = 64'h????????????????;
	            end
	    endcase
	    end
	    assign Zero = (ALU_result == 0) ? 1'b1:1'b0;
	    assign Error_Flag = error_flag;
	        
endmodule