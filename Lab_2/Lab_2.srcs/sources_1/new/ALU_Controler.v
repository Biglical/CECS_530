`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Jeremy Blaire
// 
// Create Date: 08/28/2018 07:50:46 PM
// Module Name: four2one_Mux
// Project Name: Lab 1
// Description: Basic ALU with the purpose of being a starting point for a Leg8 processor
// 
// 
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////

module LEGv8ALU_control (ALU_operation, Opcode_field, ALU_op);
input [1:0] ALU_op;
input [10:0] Opcode_field;
output reg [3:0] ALU_operation;

always @ (ALU_op, Opcode_field) begin
    case(ALU_op)
    	2'b00: Opcode_field <= 0010;
    	2'b01:
			case(Opcode_field)
				11'b10001011000:Opcode_field <= 0010;
				11'b11001011000:Opcode_field <= 0110;
				11'b10001010000:Opcode_field <= 0000;
				11'b10101010000:Opcode_field <= 0001;
			endcase
end