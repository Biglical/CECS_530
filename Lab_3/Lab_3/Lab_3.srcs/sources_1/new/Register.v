`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Jeremy Blaire
// 
// Create Date: 09/13/2018 07:55:18 PM
// Design Name: 
// Module Name: RegisterTB
// Project Name: Lab_3
// Target Devices: 
// Tool Versions: 
// Description: 2 port 32 wide register 64 bit register test
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////





module regfile (clk, reset, wr_en, rd_addr_1, rd_addr_2,
wr_addr, wr_data, rd_data_1, rd_data_2);
input clk, reset, wr_en;
input [4:0] rd_addr_1, rd_addr_2, wr_addr;
input [63:0] wr_data;
output reg [63:0] rd_data_1, rd_data_2;

reg [63:0] RF[0:31]; //internal registers
reg [6:0] i;

always @(posedge clk or posedge reset) begin

	if (reset) begin // Clear all registers
		for (i = 0; i < 32; i = i + 1) begin
			RF[i] = 0;	
		end
	end
	if(wr_en && ~reset) begin //Set if wr_en and not reset
		RF[wr_addr] <= wr_data;
	end

	rd_data_1 <= RF[rd_addr_1]; // Blocking statement so reset reads will be zero, or written content
	rd_data_2 <= RF[rd_addr_2];

end

endmodule
