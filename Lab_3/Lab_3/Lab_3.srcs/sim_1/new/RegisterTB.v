`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Jeremy Blaire
// 
// Create Date: 09/13/2018 07:55:18 PM
// Design Name: 
// Module Name: RegisterTB
// Project Name: Lab_3
// Target Devices: 
// Tool Versions: 
// Description: 2 port 32 wide register 64 bit register tets bench
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterTB();

reg clk, reset, wr_en;
reg [4:0] rd_addr_1, rd_addr_2, wr_addr;
reg [63:0] wr_data;
wire [63:0] rd_data_1, rd_data_2;

regfile register(clk, reset, wr_en, rd_addr_1, rd_addr_2, wr_addr, wr_data, rd_data_1, rd_data_2);

initial begin
rd_addr_1 <= 5; //Initlize Variables
rd_addr_2 <= 10;
wr_addr <=5;
wr_data <= 64'h5555555555555555;
wr_en <=1;
reset <=1;
clk <=0;
#1;

reset <=0; // reset should take president over write
clk <=1;
#1;
clk <=0;
#1;
clk <=1; // show 5555's are written to address 5 and come out read address 1
#1;
clk <=0;
wr_addr <=10;
wr_data <= 64'haaaaaaaaaaaaaaaa;
wr_en <=1;
#1;
clk <=1; // Load all aaaa's into register 10, rd_addr_2 should be all aaaa's
#1;
clk <=0;
wr_addr <=11;
wr_data <= 64'hdeadbeefdeadbeef;
wr_en <=1;
rd_addr_1 <=11;
#1;
clk <=1; // Load all deadbeef's into register 11, rd_addr_2 should be all aaaa's
#1;
clk <=0;
wr_addr <=12;
wr_data <= 64'hffffffffffffffff;
wr_en <=1;
rd_addr_1 <=12;
#1;
clk <=1; // Load all deadbeef's into register 11, rd_addr_2 should be all aaaa's
#1;
clk <=0;
wr_addr <=12;
wr_data <= 64'h10101010101010101010;
wr_en <=0;
rd_addr_1 <=12;
#1;
clk <=1; // Load all deadbeef's into register 11, rd_addr_2 should be all aaaa's
#1;
end

endmodule
