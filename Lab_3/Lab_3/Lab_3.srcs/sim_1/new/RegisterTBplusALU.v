`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Jeremy Blaire
// 
// Create Date: 09/13/2018 07:55:18 PM
// Design Name: 
// Module Name: RegisterTB
// Project Name: Lab_3
// Target Devices: 
// Tool Versions: 
// Description: 2 port 32 wide register 64 bit register tets bench
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterTBplusALU();

reg clk, reset, wr_en;
reg [4:0] rd_addr_1, rd_addr_2, wr_addr;
reg [63:0] wr_data;
wire [63:0] A, B;
reg [1:0] ALU_Op;
reg [10:0] Opcode_field;
wire [3:0] ALU_operation;
wire Zero;
wire Invalid;
wire [63:0] ALU_result;
ALU_Control ALUCONTROL(ALU_Op, Opcode_field, ALU_operation);
LEGv8ALU ALUTEST(ALU_operation, A, B, ALU_result, Zero, Invalid);
regfile register(clk, reset, wr_en, rd_addr_1, rd_addr_2, wr_addr, wr_data, A, B);





initial begin
    ALU_Op <= 2'b10;
    reset <=0;
    Opcode_field <=11'b10001011000; //add
	clk <= 0;
	wr_addr <=10;
	rd_addr_1 <= 10;
    rd_addr_2 <= 11;
	wr_data <= 64'haaaaaaaaaaaaaaaa;
	wr_en <=1;	
	#1;
	clk <= 1;
	#1;
	clk <=0;
	wr_addr <=11;
	wr_data <= 64'h1111111111111111;
	wr_en <=1;
	#1;
	clk <=1;
	#1
	clk <=0;
	#1;
	clk <=1; // result should be aaa.. +1111... or bbbb...



end

endmodule
