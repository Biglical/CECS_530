`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Jeremy Bliare
// 
// Create Date: 08/29/2018 10:25:37 AM
// Design Name: Lab 1 test bench
// Module Name: four2ount_Mux_testbench
// Project Name: Lab 1
// Description: Test bench to test 4 to 1 multiplexor 
// Revision 0.01 - File Created
// 
//////////////////////////////////////////////////////////////////////////////////


module four2ount_Mux_testbench();

reg [0:1] select;
reg In0, In1, In2, In3;
wire out; 

// Function to be tested
four2one_Mux mux( select, In0, In1, In2, In3, out);


initial begin
	// Set all values for initial value and test seclect 3 sets out to 1
	In0 <=0;
	In1 <=1;
	In2 <=0;
	In3 <=1;
	select <=3;
	#1;

	// Set In3 to verify that out sets on change
	In3 <=0;
	#1;

	// Set select to verify that out sets on change
	select <=2;
	#1;

	// Set In2 to verify that out sets on change
	In2 <=1;
	#1;

	// Set select to verify that out sets on change
	select <=1;
	#1;

	// Set In1 to verify that out sets on change
	In1 <=0;
	#1;

	// Set select to verify that out sets on change
	select <=0;
	#1;

	// Set In1 to verify that out sets on change
	In0 <=1;
	#1;

end

endmodule
