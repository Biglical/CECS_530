`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Jeremy Blaire
// 
// Create Date: 08/28/2018 07:50:46 PM
// Module Name: four2one_Mux
// Project Name: Lab 1
// Description: Standard Implementation of a 4 to 1 multiplexer only intended for simulation
// 
// 
// Revision 0.01 - File Created
// Additional Comments: Intent of this program is to verify knowledge of the Vivado environment for development and simulation in Verilog
// 
//////////////////////////////////////////////////////////////////////////////////


module four2one_Mux(
	// Declare module IO
    input [0:1] select,
    input In0,
    input In1,
    input In2,
    input In3,
    // reg because out should be held continuously 
    output reg out
    );
    
    // all input change will evaluate case statement
    always @(In0, In1, In2, In3, select)
    case(select)
    0: out <= In0; //non-blocking assignment for out
    1: out <= In1;
    2: out <= In2;
    3: out <= In3;
    endcase
endmodule
